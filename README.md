# Spotify 64x64rgb

Display Spotify cover in 64x64 rgb matrix hub-75e with PxMatrix and arduino-spotify-api

This code is an adaptation of the original [albumArtMatrix.ino](https://github.com/witnessmenow/arduino-spotify-api/blob/master/examples/esp32/displayAlbumArt/albumArtMatrix/albumArtMatrix.ino) code developed by Brian Lough https://twitter.com/witnessmenow

![El Caribefunk](https://gitlab.com/betojsp/spotify-64x64rgb/-/raw/af0e3f271778e5841f5388011f17d7f9f874521b/images/elcaribefunk-01.jpeg)
